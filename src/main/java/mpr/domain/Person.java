package mpr.domain;

import java.util.ArrayList;
import java.util.List;

public class Person extends Entity {

    private String firstName;
    private String surname;

    private Address address;
    private Integer age;
    private List<Role> roles = new ArrayList<Role>();

    /**
     * @return mpr.domain.Role[] roles
     */
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    /**
     * @param Role[] roles
     */
    public void addRoles(Role role) {
        this.roles.add(role);
    }

    /**
     * @return addressId
     */
    public Address getAdress() {
        return address;
    }

    /**
     * @param address
     */
    public void addAdress(Address address) {
        this.address = address;
    }

    /**
     * @return String
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param String surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param String firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
