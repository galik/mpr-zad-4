package mpr.domain;

/**
 * @author s13128
 */
public class Address extends Entity {

    private String countryName;
    private String provinceName;
    private String cityName;
    private String streetName;

    /**
     * @return String
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * @param name
     */
    public void setCountryName(String name) {
        this.countryName = name;
    }

    /**
     * @return String
     */
    public String getProvinceName() {
        return provinceName;
    }

    /**
     * @param name
     */
    public void setProvinceName(String name) {
        this.provinceName = name;
    }

    /**
     * @return String
     */
    public String getCityName() {
        return this.cityName;
    }

    /**
     * @param name
     */
    public void setCityName(String name) {
        this.cityName = name;
    }

    /**
     * @return String
     */
    public String getStreetName() {
        return this.streetName;
    }

    /**
     * @param name
     */
    public void setStreetName(String name) {
        this.streetName = name;
    }

}
