package mpr.domain;

/**
 *
 * @author s13128
 */
public class Permission extends Entity {

    private Integer permissionLevel;
    private String permissionName;

    /**
     * @return Integer
     */
    public Integer getPermissionLevel() {
        return permissionLevel;
    }

    /**
     * @param Integer permissionLevel
     */
    public void setPermissionLevel(Integer permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    /**
     * @return String
     */
    public String getPermissionName() {
        return permissionName;
    }

    /**
     * @param String permissionName
     */
    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }
}
