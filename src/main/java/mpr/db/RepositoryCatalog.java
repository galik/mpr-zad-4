package mpr.db;

/**
 * Created by galik on 22.01.2016.
 */
public interface RepositoryCatalog {


    public AddressRepository address();

    public PermissionRepository permission();

    public RoleRepository role();

    public PersonRepository person();

    public RolePermissionRepository rolePermission();

    public PersonRoleRepository personRole();
}
