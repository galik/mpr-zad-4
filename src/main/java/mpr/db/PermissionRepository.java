package mpr.db;

import mpr.domain.Permission;

import java.util.List;

/**
 * Created by galik on 21.01.2016.
 */
public interface PermissionRepository extends Repository<Permission> {

    List<Permission> withPermissionLevel(Integer permissionLevel, PagingInfo page);
    List<Permission> withPermissionName(String permissionName, PagingInfo page);
}
