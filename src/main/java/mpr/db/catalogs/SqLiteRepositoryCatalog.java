package mpr.db.catalogs;

import mpr.db.*;
import mpr.db.repositories.*;

import java.sql.Connection;

/**
 * Created by galik on 21.01.2016.
 */
public class SqLiteRepositoryCatalog implements RepositoryCatalog{

    Connection connection;

    public SqLiteRepositoryCatalog(Connection connection) {
        this.connection = connection;
    }

    public AddressRepository address() {
        return new SqLiteAddressRepository(connection);
    }

    public PermissionRepository permission() {
        return new SqLitePermissionRepository(connection);
    }

    public RoleRepository role() {
        return new SqLiteRoleRepository(connection);
    }

    public PersonRepository person() {
        return new SqLitePersonRepository(connection);
    }

    public RolePermissionRepository rolePermission() {
        return new SqLiteRolePermissionRepository(connection);
    }

    public PersonRoleRepository personRole() {
        return new SqLitePersonRoleRepository(connection);
    }

}
