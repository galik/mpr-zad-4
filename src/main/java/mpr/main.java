package mpr;

import mpr.db.PagingInfo;
import mpr.db.PersonRepository;
import mpr.db.RepositoryCatalog;
import mpr.db.catalogs.SqLiteRepositoryCatalog;
import mpr.db.repositories.SqLitePersonRepository;
import mpr.db.unitofwork.UnitOfWork;
import mpr.domain.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by galik on 21.01.2016.
 */
public class main {

    public static void main(String[] args) {

        String dbUrl = "jdbc:sqlite:mpr.db";
        Connection connection;

        try {
            connection = DriverManager.getConnection(dbUrl);
            RepositoryCatalog catalogOf = new SqLiteRepositoryCatalog(connection);

            //Dodawanie do bazy
//            Permission permission;
//            permission = new Permission();
//            permission.setPermissionLevel(1);
//            permission.setPermissionName("Boss");
//            catalogOf.permission().add(permission);
//
//            Role role;
//            role = new Role();
//            role.setRoleName("Rola");
//            role.addRoleSet(permission);
//            catalogOf.role().add(role);
//
//            RolePermission rolePermission = new RolePermission();
//            rolePermission.setPermissionId(1);
//            rolePermission.setRoleId(1);
//            catalogOf.rolePermission().add(rolePermission);
//
//            Address address;
//            address = new Address();
//            address.setCityName("elo");
//            address.setCountryName("ssss");
//            address.setProvinceName("sdsdsada");
//            address.setStreetName("jakas");
//
//            Person person;
//            person = new Person();
//            person.setFirstName("Piotr");
//            person.setSurname("G");
//            person.addAdress(address);
//            person.addRoles(role);
//            catalogOf.person().add(person);

            //Pobieranie z bazy
//            Person pp = catalogOf.person().withId(1);
//            Address ad = pp.getAdress();
//            List<Role> rl = pp.getRoles();
//            List<Permission> permissions;
//            System.out.println(pp.getId()+" "+pp.getFirstName()+" "+pp.getSurname());
//            System.out.println(ad.getCityName()+" "+ad.getCountryName()+" "+ad.getProvinceName()+" "+ad.getStreetName());
//            for (Role r : rl) {
//                permissions = r.getPermissionSet();
//                System.out.println(r.getId()+" "+r.getRoleName());
//                //TODO
//                //* Dodać do roli pobieranie uprawnień
//                for (Permission per : permissions) {
//                    System.out.println(per.getId()+" "+per.getPermissionName()+" "+per.getPermissionLevel());
//                }
//            }

            UnitOfWork uow = new UnitOfWork(connection);

            Person person = catalogOf.person().withId(1);

            person.setFirstName("Piotrek");

//            catalogOf.person().modify(person);
            uow.markAsDirty(person, new SqLitePersonRepository(connection));
            uow.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
